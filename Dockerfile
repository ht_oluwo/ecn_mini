FROM python:3.9.10

RUN mkdir -p /home/app

# create the app user
RUN addgroup djangouser && adduser --system --no-create-home djangouser --ingroup djangouser

# create the appropriate directoriesx
ENV APP_HOME=/home/app/

WORKDIR $APP_HOME

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install psycopg2 dependencies
RUN apt-get update -y \
    && apt-get install -y libffi-dev python3-setuptools libjpeg-dev zlib1g-dev

# install dependencies
RUN python -m pip install --upgrade pip

COPY ./requirements.txt $APP_HOME

RUN pip install -r requirements.txt

COPY . $APP_HOME

RUN mkdir -p staticfiles

RUN mkdir -p media

# chown all the files to the app user
RUN chown -R djangouser:djangouser $APP_HOME

# change to the app user
USER djangouser