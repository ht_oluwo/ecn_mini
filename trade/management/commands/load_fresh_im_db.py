from django.core.management.base import BaseCommand, CommandError

from ...utils import load_fresh_trade_in_im_db


class Command(BaseCommand):

    def handle(self, *args, **options):
        load_fresh_trade_in_im_db()
