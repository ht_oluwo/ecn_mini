from django.db import models

from base.models import BaseModel
from crm.models import Client, Security

# Create your models here.

TRADE_ORDER_TYPE = (
    ('Buy', 'Buy'),
    ('Sell', 'Sell'),
)

TRADE_STATUS = (
    ('Pending', 'Pending'),
    ('Partial', 'Partial'),
    ('Matched', 'Matched'),
    ('Cancelled', 'Cancelled'),
    ('Market-Cancelled', 'Market-Cancelled'),
    ('Closed', 'Closed'),
)


class ClientOrderRequest(BaseModel):
    tid = models.CharField(max_length=100, unique=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    security = models.ForeignKey(Security, on_delete=models.CASCADE)
    units = models.PositiveIntegerField(default=0)
    matched_qty = models.PositiveIntegerField(default=0)
    volume_per_unit = models.PositiveIntegerField(default=0)
    order_type = models.CharField(max_length=20, choices=TRADE_ORDER_TYPE)
    order_price = models.DecimalField(
        max_digits=20, decimal_places=2)  # price to buy or to sell
    status = models.CharField(
        max_length=30, choices=TRADE_STATUS, default="Pending")

    security_location_code = models.CharField(max_length=250, null=True, blank=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return "%s ----  %s" % (self.client.cid, self.security.code)

    @property
    def cid(self):
        return self.client.cid


class MatchedOrder(BaseModel):
    matched_id = models.CharField(max_length=130, unique=True)
    order = models.ForeignKey(
        ClientOrderRequest, on_delete=models.CASCADE, related_name="matchedorders")
    matched_with_order = models.ForeignKey(ClientOrderRequest, on_delete=models.CASCADE,
                                           related_name="matched_with_order")
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    matched_units = models.PositiveIntegerField()
    matched_price = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    is_processed = models.BooleanField(default=False)
    processed_on = models.DateTimeField(blank=True, null=True)

    total_order_price = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)

    location_breakdown = models.JSONField(null=True, blank=True)
    is_audited = models.BooleanField(default=False)
    audit_note = models.TextField(default='')

    class Meta:
        unique_together = (('order', 'matched_with_order'),)

    def __str__(self):
        return "%s - %s -%s" % (self.order.tid, self.matched_with_order.tid, self.matched_units)

    @property
    def order_type(self):
        return self.order.order_type


class MatchBook(BaseModel):
    buy_tid = models.CharField(max_length=50)
    sell_tid = models.CharField(max_length=50)
    security_code = models.CharField(max_length=20)
    matched_qty = models.PositiveIntegerField()
    matched_price = models.DecimalField(max_digits=20, decimal_places=2)
    matched_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"{self.buy_tid}-{self.sell_tid}"
