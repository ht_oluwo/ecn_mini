from django.urls import path

from .views import matched_order_audit_report, create_order, cor_list, open_orders

app_name = 'trade'

urlpatterns = [
    path('matched-order-audit-report', matched_order_audit_report, name='matched_order_audit_report'),
    path('create-order', create_order, name='create_order'),
    path('', cor_list, name='cor_list'),
    path('open-orders', open_orders, name='open_orders'),
]