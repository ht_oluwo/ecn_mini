from django import forms

from .models import ClientOrderRequest


class CreateCORForm(forms.ModelForm):
    class Meta:
        model = ClientOrderRequest
        fields = [
            'client', 'security', 'units', 'order_type', 'order_price',
        ]
        widgets = {
            'client': forms.Select(attrs={'class': "form-control"}),
            'security': forms.Select(attrs={'class': "form-control"}),
            'units': forms.NumberInput(attrs={'class': "form-control"}),
            'order_price': forms.TextInput(attrs={'class': "form-control"}),
            'order_type': forms.Select(attrs={'class': "form-control"}),
        }