import json

from decimal import Decimal
from dateutil.parser import parse

from base.redis_conn import open_redis_connection, close_redis_connection

from .models import ClientOrderRequest


def serialize_client_order_request(client_order_request: ClientOrderRequest):
    return {
        'tid': client_order_request.tid,
        'cid': client_order_request.cid,
        'security_code': client_order_request.security.code,
        'units': client_order_request.units,
        'matched_qty': client_order_request.matched_qty,
        'volume_per_unit': client_order_request.volume_per_unit,
        'order_type': client_order_request.order_type,
        'order_price': str(client_order_request.order_price),
        'status': client_order_request.status,
        'security_location_code': client_order_request.security_location_code,
        'created': str(client_order_request.created),
    }

def push_client_order_request_to_im_db(instance):
    redis = open_redis_connection()
    redis.sadd('open_trades', dump_for_im_db(serialize_client_order_request(instance)))
    close_redis_connection(redis)

def dump_for_im_db(data):
    return json.dumps(data)

def get_open_trades():
    redis_conn = open_redis_connection()
    data = redis_conn.smembers('open_trades')
    close_redis_connection(redis_conn)
    return list(map(json.loads, data))

def clean_trades(trades):
    for trade in trades:
        trade['order_price'] = Decimal(trade['order_price'])
        trade['created'] = parse(trade['created'])
    return trades

def load_fresh_trade_in_im_db():
    redis_conn = open_redis_connection()
    redis_conn.delete('open_trades')
    qs = ClientOrderRequest.objects.filter(status__in=["Pending", "Partial", ]).order_by('-created')
    for cor in qs:
        redis_conn.sadd('open_trades', dump_for_im_db(serialize_client_order_request(cor)))
    close_redis_connection(redis_conn)

def update_trade_in_im_db(trade):
    redis_conn = open_redis_connection()
    data = redis_conn.smembers('open_trades')
    json_data = list(map(json.loads, data))
    redis_conn.delete('open_trades')
    for trade_data in json_data:
        # Check if trade is the object in iteration
        if trade_data['tid'] != trade.tid:
            redis_conn.sadd('open_trades', dump_for_im_db(trade_data))

    if trade.status in ["Pending", "Partial", ]: # if trade is still open
        redis_conn.sadd('open_trades', dump_for_im_db(serialize_client_order_request(trade)))

    close_redis_connection(redis_conn)
