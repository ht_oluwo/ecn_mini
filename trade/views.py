from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.utils.crypto import get_random_string
from django.contrib import messages

from .forms import CreateCORForm
from .models import ClientOrderRequest, MatchedOrder
from .utils import get_open_trades, clean_trades

# Create your views here.


def matched_order_audit_report(request):
    context = {
        'objects': MatchedOrder.objects.all()
    }
    return render(request, 'trade/index.html', context)


def create_order(request):

    if request.method == 'POST':
        form = CreateCORForm(request.POST)
        if form.is_valid():
            cor = form.save(commit=False)
            cor.tid = get_random_string(10, "0123456789")
            cor.volume_per_unit = cor.security.volume_per_unit
            cor.save()
            messages.success(request, f"You have place a {cor.order_type.lower()} order for {cor.security} successfully")
            return HttpResponseRedirect(reverse('trade:cor_list'))
    else:
        form = CreateCORForm()

    context = {
        'form': form,
    }
    return render(request, 'trade/create_order.html', context)


def cor_list(request):
    context = {
        'cors': ClientOrderRequest.objects.all(),
    }
    return render(request, 'trade/order_list.html', context)


def open_orders(request):
    context = {
        'open_orders': clean_trades(get_open_trades()),
    }
    return render(request, 'trade/open_order_list.html', context)