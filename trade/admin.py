from django.contrib import admin

from .models import ClientOrderRequest, MatchedOrder, MatchBook

# Register your models here.

class ClientOrderRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'tid', 'units', 'matched_qty', 'order_type', 'created', 'updated', ]


admin.site.register(ClientOrderRequest, ClientOrderRequestAdmin)
admin.site.register(MatchedOrder)
admin.site.register(MatchBook)
