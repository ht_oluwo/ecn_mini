from django.db.models.signals import post_save
from django.dispatch import receiver

from trade.models import ClientOrderRequest

from .tasks import match_orders_spot
from .utils import update_trade_in_im_db


@receiver(post_save, sender=ClientOrderRequest)
def send_to_im_db(sender, instance: ClientOrderRequest, created, **kwargs):
    update_trade_in_im_db(instance)
    match_orders_spot()
