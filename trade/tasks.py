from itertools import groupby
from operator import itemgetter
from time import perf_counter

from django.db.models import Q
from django.utils import timezone

from crm.models import Security
from crm.tasks import post_settlement_operation
from crm.utils import client_wallet_operation, client_portfolio_operation
from ecn_mini.celery import app
from trade.models import ClientOrderRequest, MatchedOrder, MatchBook

from .utils import clean_trades, get_open_trades


@app.task
def match_orders_spot():
    start = perf_counter()
    open_trades = list(clean_trades(get_open_trades()))
    open_trades_by_security = sorted(open_trades, key=itemgetter('security_code'))
    for security_code, security_trades in groupby(open_trades_by_security, itemgetter('security_code')):
        # Get buy orders
        security_trades = list(security_trades)
        buy_orders = filter(lambda trade: trade['order_type'] == 'Buy', sorted(security_trades, key=itemgetter('created')))
        sell_orders = filter(lambda trade: trade['order_type'] == 'Sell', sorted(security_trades, key=lambda trade: (trade['order_price'], trade['created'])))

        match_book = []
        for b_order in buy_orders:
            qty = b_order['units'] - b_order['matched_qty']
            proceed = False
            while not proceed:
                price = b_order['order_price']
                tid = b_order['tid']
                if security_code.startswith('D'):
                    match = next((l for l in sell_orders if
                                  l['order_price'] <= price and l['units'] > 0 and b_order['cid'] != l['cid']
                                  and b_order['security_location_code'] == l['security_location_code']), None)
                else:
                    match = next((l for l in sell_orders if
                                  l['order_price'] <= price and l['units'] > 0 and b_order['cid'] != l['cid']), None)
                """ deduct units """
                if match:
                    m_units = match['units'] - match['matched_qty']
                    if qty <= m_units:
                        match['units'] -= qty
                        matched_qty = qty
                        proceed = True
                    else:
                        match['units'] = 0
                        matched_qty = m_units
                    ''' Create match book'''
                    match_book.append({'buy_tid': tid, 'sell_tid': match['tid'], 'matched_qty': matched_qty,
                                       'security_code': match['security_code'], 'matched_date': timezone.now(),
                                       'matched_price': match['order_price']})
                    """ reduce the qty remaining to match"""
                    qty -= matched_qty
                else:
                    proceed = True

        for m in match_book:
            try:
                buy_order = ClientOrderRequest.objects.get(
                    tid=m['buy_tid'], order_type="Buy")
                sell_order = ClientOrderRequest.objects.get(
                    tid=m['sell_tid'], order_type="Sell")
            except:
                continue
            # Buy
            ordered_units = buy_order.units
            remaining_units = ordered_units - buy_order.matched_qty
            if remaining_units > m['matched_qty']:
                buy_order.matched_qty += m['matched_qty']
                buy_order.status = 'Partial'
                buy_order.save()

            elif remaining_units == m['matched_qty']:
                buy_order.matched_qty += m['matched_qty']
                buy_order.status = 'Matched'
                buy_order.save()

            # Sell
            units_to_sell = sell_order.units
            remaining_unitsx = units_to_sell - sell_order.matched_qty
            if remaining_unitsx > m['matched_qty']:
                sell_order.matched_qty += m['matched_qty']
                sell_order.status = 'Partial'
                sell_order.save()
            elif remaining_unitsx == m['matched_qty']:
                sell_order.matched_qty += m['matched_qty']
                sell_order.status = 'Matched'
                sell_order.save()
                """ Notify Market Data"""
            # Create match_book
            matched_units = m['matched_qty']

            security_obj: Security = sell_order.security

            # if security_obj.security_type == "Dawa" and not security_obj.location_basis_code:
            #     print("Location basis code not found for security")
            #     continue

            # Create match_book
            try:
                matched_bb = MatchBook.objects.create(buy_tid=m['buy_tid'], sell_tid=m['sell_tid'],
                                                      matched_qty=m['matched_qty'],
                                                      matched_price=m['matched_price'], matched_date=m['matched_date'],
                                                      security_code=m['security_code'],
                                                      )
            except:
                matched_bb = MatchBook.objects.get(
                    buy_tid=m['buy_tid'], sell_tid=m['sell_tid'])

            total_volume = matched_units * security_obj.volume_per_unit

            """ Buy matched order """
            try:
                buy_matched_id = "%s-%s" % (buy_order.tid, sell_order.tid)
                buy_matched_order = MatchedOrder.objects.create(order=buy_order, matched_with_order=sell_order,
                                                 client=buy_order.client,
                                                 matched_units=m['matched_qty'], matched_id=buy_matched_id,
                                                 matched_price=m['matched_price'])

            except Exception as e:
                # print(e)
                matched_bb = MatchedOrder.objects.get(
                    order=buy_order, matched_with_order=sell_order)

            """ Sell matched order """
            try:
                sell_matched_id = "%s-%s" % (sell_order.tid,
                                             buy_order.tid)

                sell_matched_order = MatchedOrder.objects.create(order=sell_order, matched_with_order=buy_order,
                                                  client=sell_order.client,
                                                  matched_units=m['matched_qty'], matched_id=sell_matched_id,
                                                  matched_price=m['matched_price'])

            except Exception as e:
                # print(e)
                matched_bbx = MatchedOrder.objects.get(
                    order=sell_order, matched_with_order=buy_order)

        try:
            process_match_order_notification(buy_matched_order.matched_id)
        except Exception as e:
            pass

        try:
            process_match_order_notification(sell_matched_order.matched_id)
        except Exception as e:
            pass

    finish = perf_counter()
    print(f"Finished matching in {finish - start:0.4f} seconds")


@app.task
def match_orders_spot_from_db():
    start = perf_counter()
    all_securities = ClientOrderRequest.objects.filter(
        Q(status="Pending") | Q(status="Partial")).order_by('order_price', '-created').values('security__code')
    for security in all_securities:
        security_code: str = security['security__code']
        # print('----------------------------------- SPOT %s -----------------------------------' % security_code)
        buy_orders = ClientOrderRequest.objects.filter(order_type="Buy", security__code=security_code,
                                                       ).filter(
            Q(status="Pending") | Q(status="Partial")).order_by('created').values('tid', 'client__cid', 'security__code',
                                                                                  'units', 'matched_qty', 'order_type',
                                                                                  'order_price', 'security_location_code',
                                                                                  )
        sell_orders = ClientOrderRequest.objects.filter(security__code=security_code,
                                                       ).filter(
            Q(status="Pending") | Q(status="Partial")).order_by('order_price', 'created').values('tid', 'client__cid',
                                                                                                 'security__code',
                                                                                                 'units', 'matched_qty',
                                                                                                 'order_type',
                                                                                                 'order_price', 'security_location_code',
                                                                                                 )

        match_book = []
        for b_order in buy_orders:
            qty = b_order['units'] - b_order['matched_qty']
            proceed = False
            while not proceed:
                price = b_order['order_price']
                tid = b_order['tid']
                if security_code.startswith('D'):
                    match = next((l for l in sell_orders if
                                  l['order_price'] <= price and l['units'] > 0 and b_order['client__cid'] != l['client__cid']
                                  and b_order['security_location_code'] == l['security_location_code']), None)
                else:
                    match = next((l for l in sell_orders if
                                  l['order_price'] <= price and l['units'] > 0 and b_order['client__cid'] != l['client__cid']), None)
                """ deduct units """
                if match:
                    m_units = match['units'] - match['matched_qty']
                    if qty <= m_units:
                        match['units'] -= qty
                        matched_qty = qty
                        proceed = True
                    else:
                        match['units'] = 0
                        matched_qty = m_units
                    ''' Create match book'''
                    match_book.append({'buy_tid': tid, 'sell_tid': match['tid'], 'matched_qty': matched_qty,
                                       'security_code': match['security__code'], 'matched_date': timezone.now(),
                                       'matched_price': match['order_price']})
                    """ reduce the qty remaining to match"""
                    qty -= matched_qty
                else:
                    proceed = True

        for m in match_book:
            try:
                buy_order = ClientOrderRequest.objects.get(
                    tid=m['buy_tid'], order_type="Buy")
                sell_order = ClientOrderRequest.objects.get(
                    tid=m['sell_tid'], order_type="Sell")
            except:
                continue
            # Buy
            ordered_units = buy_order.units
            remaining_units = ordered_units - buy_order.matched_qty
            if remaining_units > m['matched_qty']:
                buy_order.matched_qty += m['matched_qty']
                buy_order.status = 'Partial'
                buy_order.save()

            elif remaining_units == m['matched_qty']:
                buy_order.matched_qty += m['matched_qty']
                buy_order.status = 'Matched'
                buy_order.save()

            # Sell
            units_to_sell = sell_order.units
            remaining_unitsx = units_to_sell - sell_order.matched_qty
            if remaining_unitsx > m['matched_qty']:
                sell_order.matched_qty += m['matched_qty']
                sell_order.status = 'Partial'
                sell_order.save()
            elif remaining_unitsx == m['matched_qty']:
                sell_order.matched_qty += m['matched_qty']
                sell_order.status = 'Matched'
                sell_order.save()
                """ Notify Market Data"""
            # Create match_book
            matched_units = m['matched_qty']

            security_obj: Security = sell_order.security

            # if security_obj.security_type == "Dawa" and not security_obj.location_basis_code:
            #     print("Location basis code not found for security")
            #     continue

            # Create match_book
            try:
                matched_bb = MatchBook.objects.create(buy_tid=m['buy_tid'], sell_tid=m['sell_tid'],
                                                      matched_qty=m['matched_qty'],
                                                      matched_price=m['matched_price'], matched_date=m['matched_date'],
                                                      security_code=m['security_code'])
            except:
                matched_bb = MatchBook.objects.get(
                    buy_tid=m['buy_tid'], sell_tid=m['sell_tid'])

            total_volume = matched_units * security_obj.volume_per_unit

            """ Buy matched order """
            try:
                buy_matched_id = "%s-%s" % (buy_order.tid, sell_order.tid)
                buy_matched_order = MatchedOrder.objects.create(order=buy_order, matched_with_order=sell_order,
                                                 client=buy_order.client,
                                                 matched_units=m['matched_qty'], matched_id=buy_matched_id,
                                                 matched_price=m['matched_price'])

            except Exception as e:
                # print(e)
                matched_bb = MatchedOrder.objects.get(
                    order=buy_order, matched_with_order=sell_order)

            """ Sell matched order """
            try:
                sell_matched_id = "%s-%s" % (sell_order.tid,
                                             buy_order.tid)

                sell_matched_order = MatchedOrder.objects.create(order=sell_order, matched_with_order=buy_order,
                                                  client=sell_order.client,
                                                  matched_units=m['matched_qty'], matched_id=sell_matched_id,
                                                  matched_price=m['matched_price'])


            except Exception as e:
                # print(e)
                matched_bbx = MatchedOrder.objects.get(
                    order=sell_order, matched_with_order=buy_order)

    try:
        process_match_order_notification(buy_matched_order.matched_id)
    except Exception as e:
        pass

    try:
        process_match_order_notification(sell_matched_order.matched_id)
    except Exception as e:
        pass

    finish = perf_counter()
    print(f"Finished matching in {finish - start:0.4f} seconds")


def process_match_order_notification(tid=None):
    orders = MatchedOrder.objects.filter(matched_id=tid, is_processed=False)

    for porder in orders:
        xorder = porder.order
        if xorder.order_type == "Buy":
            security = xorder.security

            total_order_price = porder.matched_price * porder.matched_units

            client_wallet_operation(
                porder.client, total_order_price, "Debit",
                extra_note=f"A total of {porder.matched_units} out of {xorder.units}(s) have matched from your "
                           f"{security.code} {xorder.order_type} order. Order tid is %s" % porder.tid,
                ref_id=porder.matched_id,
                audit=False
            )

            client_portfolio_operation(
                porder.client, security, porder.matched_units, 'Credit',
                extra_note="Portfolio account credited with %s for matched order with matched_id %s. Order ID is %s " % (
                porder.matched_units, porder.matched_id, xorder.tid), ref_id=porder.matched_id
            )

            porder.is_processed = True
            porder.processed_on = timezone.now()
            porder.total_order_price = total_order_price
            porder.save()
        elif xorder.order_type == "Sell":
            security = xorder.security
            total_order_price = porder.matched_price * porder.matched_units

            client_wallet_operation(
                porder.client, total_order_price, "Credit",
                extra_note=f"A total of {porder.matched_units} out of {xorder.units}(s) have matched from your "
                           f"{security.code} {xorder.order_type} order. Order tid is %s" % porder.tid,
                ref_id=porder.matched_id,
                audit=False
            )

            client_portfolio_operation(
                porder.client, security, porder.matched_units, 'Debit',
                extra_note="Portfolio account debited with %s for matched order with matched_id %s. Order ID is %s " % (
                                           porder.matched_units, porder.matched_id, xorder.tid),
                ref_id=porder.matched_id
            )

            porder.is_processed = True
            porder.processed_on = timezone.now()
            porder.total_order_price = total_order_price
            porder.save()
        post_settlement_operation.delay(porder.matched_id)