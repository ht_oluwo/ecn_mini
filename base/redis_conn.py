from django.conf import settings

import redis


def open_redis_connection():
    redis_conn = redis.from_url(settings.CELERY_BROKER_URL, db=1, decode_responses=True)
    return redis_conn

def close_redis_connection(connector):
    connector.close()