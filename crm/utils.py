from decimal import Decimal

from crm.models import ClientWallet, WalletOperationLogging, ClientPortfolio, PortfolioLogging


def get_client_wallet(client):
    client_wallet, _ = ClientWallet.objects.get_or_create(
        client=client)
    return client_wallet


def client_wallet_operation(client, amount, op_type, extra_note="", ref_id="", audit=False):
    client_account = get_client_wallet(client)
    total_before = client_account.total_balance
    available_before = client_account.available_balance
    lien_before = client_account.lien_balance
    amount = Decimal(amount)
    if op_type == 'Debit':
        client_account.total_balance -= amount
        client_account.available_balance -= amount
    elif op_type == 'Credit':
        client_account.total_balance += amount
        client_account.available_balance += amount
    elif op_type == 'Lien':
        client_account.available_balance -= amount
        client_account.lien_balance += amount
    elif op_type == 'UnLien':
        client_account.available_balance += amount
        client_account.lien_balance -= amount
    elif op_type == 'UnLien-Debit':
        client_account.lien_balance -= amount
        client_account.total_balance -= amount
    client_account.save()
    client_account.refresh_from_db()

    """ Now create transaction history"""
    client_wallet_operation_logging(client_account.client, op_type, amount, total_before, available_before, lien_before,
                                    client_account.total_balance, client_account.available_balance,
                                    client_account.lien_balance, extra_note, ref_id, audit=audit)
    return


def client_wallet_operation_logging(client, transaction_type, amount, total_before, available_before, lien_before,
                                    total_after, available_after, lien_after, extra_note, ref_id, audit=False):
    WalletOperationLogging.objects.create(client=client, transaction_type=transaction_type, amount=amount,
                                         extra_note=extra_note,
                                         ref_id=ref_id, total_before=total_before, available_before=available_before,
                                         lien_before=lien_before, total_after=total_after,
                                         available_after=available_after,
                                         lien_after=lien_after, is_audited=audit)


def get_client_portfolio(client, security):
    client_portfolio, _ = ClientPortfolio.objects.get_or_create(
        client=client, security=security)
    return client_portfolio


def client_portfolio_operation(client, security, units, op_type, extra_note="", ref_id=""):
    client_portfolio = get_client_portfolio(client, security)
    total_before = client_portfolio.total_units
    available_before = client_portfolio.available_units
    lien_before = client_portfolio.units_on_hold
    if op_type == 'Debit':
        client_portfolio.total_units -= units
        client_portfolio.available_units -= units
    elif op_type == 'Credit':
        client_portfolio.total_units += units
        client_portfolio.available_units += units
    elif op_type == 'Lien':
        client_portfolio.available_units -= units
        client_portfolio.units_on_hold += units
    elif op_type == 'UnLien':
        client_portfolio.available_units += units
        client_portfolio.units_on_hold -= units
    elif op_type == 'UnLien-Debit':
        client_portfolio.units_on_hold -= units
        client_portfolio.total_units -= units
    client_portfolio.save()
    client_portfolio.refresh_from_db()

    """ Now create transaction history"""
    client_portfolio_operation_logging(client_portfolio.client, security, op_type, units, total_before, available_before, lien_before,
                                    client_portfolio.total_units, client_portfolio.available_units,
                                    client_portfolio.units_on_hold, extra_note, ref_id)
    return


def client_portfolio_operation_logging(client, security, transaction_type, units, total_before, available_before, lien_before,
                                    total_after, available_after, lien_after, extra_note, ref_id, audit=True):
    PortfolioLogging.objects.create(client=client, security=security, transaction_type=transaction_type, units=units,
                                         extra_note=extra_note,
                                         ref_id=ref_id, total_units_before=total_before, available_units_before=available_before,
                                         lien_units_before=lien_before, total_units_after=total_after,
                                         available_units_after=available_after,
                                         lien_units_after=lien_after)

