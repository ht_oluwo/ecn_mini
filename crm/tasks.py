from ecn_mini.celery import app

from crm.models import WalletOperationLogging, PortfolioLogging
from crm.utils import client_wallet_operation, client_portfolio_operation
from trade.models import MatchedOrder


# @periodic_task(run_every=(crontab(hour=0, minute=0)), name="check_wallet_operation_periodically")
@app.task
def reverse_wallet_operations(record: WalletOperationLogging):
    if record.transaction_type == "Debit":
        client_wallet_operation(record.client, record.amount, "Credit", extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
    elif record.transaction_type == "Credit":
        client_wallet_operation(record.client, record.amount, "Debit",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
    elif record.transaction_type == "Lien":
        client_wallet_operation(record.client, record.amount, "UnLien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
    elif record.transaction_type == "UnLien":
        client_wallet_operation(record.client, record.amount, "Lien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
    elif record.transaction_type == "UnLien-Debit":
        client_wallet_operation(record.client, record.amount, "Lien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
        client_wallet_operation(record.client, record.amount, "Credit", extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}", audit=True)
    record.is_audited = True
    record.save()


# @periodic_task(run_every=(crontab(hour=0, minute=0)), name="check_wallet_operation_periodically")
@app.task
def reverse_portfolio_operations(record: PortfolioLogging):
    if record.transaction_type == "Debit":
        client_portfolio_operation(record.client, record.security, record.units, "Credit", extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")
    elif record.transaction_type == "Credit":
        client_portfolio_operation(record.client, record.security, record.units, "Debit",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")
    elif record.transaction_type == "Lien":
        client_portfolio_operation(record.client, record.security, record.units, "UnLien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")
    elif record.transaction_type == "UnLien":
        client_portfolio_operation(record.client, record.security, record.units, "Lien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")
    elif record.transaction_type == "UnLien-Debit":
        client_portfolio_operation(record.client, record.security, record.units, "Lien",
                                extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")
        client_portfolio_operation(record.client, record.security, record.units, "Credit", extra_note=f"audit auto-correction for {record.extra_note, record.ref_id}")


@app.task
def post_settlement_operation(matched_order_id: MatchedOrder):
    matched_order = MatchedOrder.objects.filter(matched_id=matched_order_id, is_audited=False).first()
    if matched_order:
        audit_note = ''
        wallet_operations = WalletOperationLogging.objects.filter(is_audited=False, ref_id=matched_order.matched_id)
        if len(wallet_operations) > 1:
            for record in wallet_operations[1:]:
                reverse_wallet_operations(record=record)
            audit_note += "Reversed multiple wallet operations"
        elif len(wallet_operations) < 1:
            if matched_order.order.order_type == "Buy" and matched_order.order.status == "Matched":
                client_wallet_operation(matched_order.client, matched_order.matched_price, "Debit",
                                        extra_note=f"audit auto-correction for Matched buy order", ref_id=f"{matched_order.matched_id}")
                audit_note += "Made actual wallet debit"
            elif matched_order.order.order_type == "Sell" and matched_order.order.status == "Matched":
                client_wallet_operation(matched_order.client, matched_order.matched_price, "Credit",
                                        extra_note=f"audit auto-correction for Matched sell order",
                                        ref_id=matched_order.matched_id)
                audit_note += "Made actual wallet credit"
        portfolio_operations = PortfolioLogging.objects.filter(ref_id=matched_order.matched_id)
        if len(portfolio_operations) > 1:
            for record in portfolio_operations[1:]:
                reverse_portfolio_operations(record)
            audit_note += "\nReversed multiple portfolio operations"
        elif len(portfolio_operations) < 1:
            if matched_order.order.order_type == "Buy" and matched_order.order.status == "Matched":
                client_portfolio_operation(matched_order.client, matched_order.order.security, matched_order.matched_units,
                                           "Credit", extra_note=f"audit auto-correction for Matched buy order",
                                           ref_id=matched_order.matched_id)
                audit_note += "\nMade actual portfolio credit"
            elif matched_order.order.order_type == "Sell" and matched_order.order.status == "Matched":
                client_portfolio_operation(matched_order.client, matched_order.order.security, matched_order.matched_units,
                                           "Debit", extra_note=f"audit auto-correction for Matched sell order",
                                           ref_id=matched_order.matched_id)
                audit_note += "\nMade actual portfolio debit"

        matched_order.is_audited = True
        matched_order.audit_note = "No problems found" if audit_note == '' else audit_note
        matched_order.save()







