from django.contrib import admin

from .models import Client, ClientWallet, ClientPortfolio, Security, Commodity, Location

# Register your models here.

admin.site.register(Commodity)
admin.site.register(Client)
admin.site.register(ClientWallet)
admin.site.register(ClientPortfolio)
admin.site.register(Security)
admin.site.register(Location)
