from django.db import models

from base.models import BaseModel


WALLET_OPERATION = (
        ("Credit", "Credit"),
        ("Debit", "Debit"),
        ("Lien", "Lien"),
        ("UnLien", "UnLien"),
        ("UnLien-Debit", "UnLien-Debit"),
    )


class Client(BaseModel):
    cid = models.CharField(max_length=60, unique=True)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300, null=True, blank=True, default="")
    email = models.EmailField()

    def __str__(self):
        return self.cid


class Commodity(BaseModel):
    code = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=120)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return "%s - %s" % (self.code, self.name)


class Location(BaseModel):
    name = models.CharField(max_length=150)
    code = models.CharField(max_length=40)

    def __str__(self):
        return "%s - %s" % (self.code, self.name)


class Security(models.Model):
    commodity = models.ForeignKey(Commodity, on_delete=models.CASCADE)
    code = models.CharField(max_length=120)
    name = models.CharField(max_length=255, null=True, blank=True)
    volume_per_unit = models.PositiveIntegerField()

    def __str__(self):
        return self.code


class ClientPortfolio(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True)
    security = models.ForeignKey(Security, on_delete=models.CASCADE, null=True)
    total_units = models.PositiveIntegerField(default=0)
    available_units = models.PositiveIntegerField(default=0)
    units_on_hold = models.PositiveIntegerField(default=0)
    updated = models.DateTimeField(auto_now=True)
    location_breakdown = models.JSONField(null=True, blank=True)

    def __str__(self):
        return "Client ID - %s " % self.client.cid


class PortfolioLogging(models.Model):
    client = models.ForeignKey(
        Client, on_delete=models.CASCADE, related_name="portfolio_logging_client")
    security = models.ForeignKey(Security, on_delete=models.SET_NULL, null=True, blank=True,
                                 related_name="portfolio_logging_security")
    total_units_before = models.PositiveIntegerField()
    available_units_before = models.PositiveIntegerField()
    lien_units_before = models.PositiveIntegerField()
    total_units_after = models.PositiveIntegerField()
    available_units_after = models.PositiveIntegerField()
    lien_units_after = models.PositiveIntegerField()
    units = models.PositiveIntegerField()
    transaction_type = models.CharField(max_length=30,
                                        choices=[('Credit', 'Credit'), ('Debit', 'Debit'), ('Set', 'Set'), ('Lien', 'Lien'), ('UnLien', 'UnLien'), ('UnLien-Debit', 'UnLien-Debit')])
    extra_note = models.TextField(null=True, blank=True)
    ref_id = models.CharField(max_length=35, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s - %s -%s - %s" % (
            self.client.cid, self.security.code,  self.available_units_before, self.available_units_after)


class ClientWallet(models.Model):
    client = models.OneToOneField(Client, on_delete=models.CASCADE)
    total_balance = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    available_balance = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    lien_balance = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    cash_advance_balance = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    cash_advance_limit = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    cash_advance_spent = models.DecimalField(
        max_digits=20, decimal_places=2, default=0)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.cid

    @property
    def cid(self):
        return self.client.cid


class WalletOperationLogging(BaseModel):
    client = models.ForeignKey(Client, null=True, blank=True, on_delete=models.CASCADE)
    transaction_type = models.CharField(max_length=20, choices=WALLET_OPERATION)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    total_before = models.DecimalField(max_digits=20, decimal_places=2)
    available_before = models.DecimalField(max_digits=20, decimal_places=2)
    lien_before = models.DecimalField(max_digits=20, decimal_places=2)
    total_after = models.DecimalField(max_digits=20, decimal_places=2)
    available_after = models.DecimalField(max_digits=20, decimal_places=2)
    lien_after = models.DecimalField(max_digits=20, decimal_places=2)
    extra_note = models.TextField(null=True, blank=True)
    ref_id = models.CharField(max_length=200, null=True, blank=True)
    is_audited = models.BooleanField(default=False)

    @property
    def cid(self):
        return self.client.cid